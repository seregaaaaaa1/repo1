﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemFor13
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> degrees = new List<string>();
            ConsoleKeyInfo control;
            while ((control = Console.ReadKey()).Key != ConsoleKey.Escape)
            {
                double qual = Convert.ToInt32(Console.ReadLine());
                double sum = 0;
                for (double i = 0; i < qual; i++)
                {
                    double temp = 1 + ((i + 1) / 10);
                    sum = sum + Math.Pow(-1, i) * temp;
                }
                Console.WriteLine(sum);
                Console.ReadKey();
            }
        }
    }
}