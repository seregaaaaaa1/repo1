﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemFor16
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> degrees = new List<string>();
            ConsoleKeyInfo control;
            while ((control = Console.ReadKey(true)).Key != ConsoleKey.Escape)
            {
                Console.WriteLine("Введите степень N, число A");
                int n = Convert.ToInt32(Console.ReadLine());
                double a = Convert.ToDouble(Console.ReadLine());
                double degreeOfNumber = 1;
                for (int i = 1; i <= n; i++)
                {
                    degreeOfNumber *= a;
                    degrees.Add($"{degreeOfNumber}");
                }
            }
            foreach (string degree in degrees)
            {
                Console.WriteLine(degree);
            }
                Console.ReadKey();
            }
        }
    }
