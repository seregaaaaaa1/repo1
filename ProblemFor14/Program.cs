﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemFor15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите N");
            int n = Convert.ToInt32(Console.ReadLine());
            int degreeOfNumber = 0;
            for (int i=1; i<=n; i++)
            {
                degreeOfNumber += 2 * i - 1;
                Console.WriteLine($"{i}, {degreeOfNumber}");
            }
            Console.ReadKey();
        }
    }
}
