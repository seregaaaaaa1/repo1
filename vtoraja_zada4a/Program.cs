﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vtoraja_zada4a
{
    class Program
    {
        static void Main()
        {
            //Console.WriteLine("Введите массу в килограммах_");
            //int M = Convert.ToInt32(Console.ReadLine());
            //int fullTons = M / 1000;
            //Console.WriteLine($"Количество полных тонн: {fullTons}");
            //Console.ReadKey();
            int number = 0;
            do
            {
                Console.WriteLine("Введите двузначное число_");
                number = Convert.ToInt32(Console.ReadLine());
            }
            while ((number >= 100) || (number < 10));
            {
                int tens = (number / 10), units = (number % 10);
                Console.WriteLine($"Десятки: {tens}\nЕдиницы: {units}");
                Console.ReadKey();
           }

            //int number = 0;
            do
            {
                Console.WriteLine("Введите трехзначное число_");
                number = Convert.ToInt32(Console.ReadLine());
            }
            while ((number < 100) || (number > 999));
            {
                int summ = ((number / 100) + ((number % 100) / 10) + (number % 10));
                int multipl = ((number / 100) * ((number % 100) / 10) * (number % 10));
                Console.WriteLine($"Сумма: {summ}\nПроизведение: {multipl}");
                Console.ReadKey();
            }
        }
    }
}
