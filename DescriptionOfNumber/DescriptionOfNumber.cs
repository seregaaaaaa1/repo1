﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescriptionOfNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> descriptions = new List<string> ();
            ConsoleKeyInfo control;
            while ((control = Console.ReadKey()).Key != ConsoleKey.Escape)
            {
                Console.WriteLine("Введите целое число");
                int number = Convert.ToInt32(Console.ReadLine());
                if (number != 0)
                {                                    
                    descriptions.Add($"Число {number}, {Parity(number)}, {PositiveOrNegative(number)}");
                }
                else
                {                                 
                    descriptions.Add($"Число {number}, {Parity(number)}, нулевое"); //коммент 2
                }
                Console.ReadKey();
            }            
            foreach (string description in descriptions)
                {
                Console.WriteLine(description);
                Console.ReadKey();
                }
        }
        static string Parity(int number)
        {
            string a = "";
            if (number % 2 == 0)
            {
               a="четное";
            }
            else
            {
               a="нечетное";
            }
            return a;
        }
        static string PositiveOrNegative(int number)
        {
            string a = "";
            if (number > 0)
            {
                a="положительное";
            }
            else if (number < 0)
            {
                a="отрицательное";
            }
            return a;
        }        
    }
}
