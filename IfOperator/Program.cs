﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfOperator
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Введите аргумент (x)");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Это не аргумент... Шутка)))");
            Console.WriteLine($"Problem24={Prob24(x)}");
            Prob25(x);
            Console.ReadKey();
        }
        static double Prob24(double x)
        {
            if (x > 0)
            {
               return 2 * Math.Sin(x);
            }
            else
            {
                return 6 - x;
            }

        }

        static void Prob25(double x)
        {
            if ((x < -2) || (x > 2))
            {
                Console.WriteLine("2*x = {0}", 2 * x);
            }

            else
            {
                Console.WriteLine("-3*x = {0}", -3 * x);
            }
        }

    }
}
