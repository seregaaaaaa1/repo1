﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberInLimits
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите любое число");
            double number = Convert.ToDouble(Console.ReadLine());
            double integerPart = Math.Floor(number);
            if (number < 0)
            {
                Console.WriteLine('0');
            }
            else if (((integerPart % 2 == 0) && (number >= integerPart)) && (number < integerPart + 1))
            {
                Console.WriteLine('1');
            }
            else
            {
                Console.WriteLine("-1");
            }         
            Console.ReadKey();         
        }
    }
}
