﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCount
{
    class Program
    {
       // int number = 0;
        static void Main(string[] args)
        {
            int number = 0;
            int result = 0;
            //int summ = 0;
            //int mult = 0;
            do
            {
               // int number = 0;
                Console.WriteLine("Введите трехзначное число");
                number = Convert.ToInt32(Console.ReadLine());
                result=ChekingOfNumber(number);
                //int number1 = number;                
            }
            while (result != 3);
            {
               int summ = SummDigitsOfNumber(number);
               int mult = MultiplicationDigitsOfNumber(number);
               Console.WriteLine($"Сумма цифр числа: {summ}, Произведение цифр числа: {mult}");
            }
            Console.ReadKey();
        }
       static int ChekingOfNumber(int number)
        {                    
            int count = 0;
            while (number != 0)
                {
                int qoutientOfDiv = number / 10;
                int remainderOFDiv = number % 10;              
                number = qoutientOfDiv;
                count = count + 1;
            }
            return count;            
        }
        static int SummDigitsOfNumber(int number)
        {
            int summ = 0;
            while (number != 0)
            {
                int qoutientOfDiv = number / 10;
                int remainderOfDiv = number % 10;
                summ = summ + remainderOfDiv;
                number = qoutientOfDiv;
            }
            return summ;
        }
        static int MultiplicationDigitsOfNumber (int number)
        {
            int mult = 1;
            while (number != 0)
            {
                int qoutientOfDiv = number / 10;
                int remainderOfDiv = number % 10;
                mult = mult * remainderOfDiv;
                number = qoutientOfDiv;
            }
            return mult;
        }
    }
}
