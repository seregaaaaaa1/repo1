﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemFor17
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> results = new List<string>();
            ConsoleKeyInfo control;
            while ((control = Console.ReadKey(true)).Key != ConsoleKey.Escape)
            {
                Console.WriteLine("Введите целое N и вещественное А ");
                int n = Convert.ToInt32(Console.ReadLine());
                double a = Convert.ToDouble(Console.ReadLine());               
                double sum = 1;
                double degreeOFNumber = 1;
                for (int i = 1; i <= n; i++)
                {
                    degreeOFNumber *= a;
                    sum += degreeOFNumber;
                    results.Add($"{sum}");
                }
                Console.WriteLine(sum);
            }
            foreach (string result in results)
            {
                Console.WriteLine(result);
            }
            Console.ReadKey();
        }
    }
}
