﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemFor15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите степень N, число A");
            int n = Convert.ToInt32(Console.ReadLine());
            double a= Convert.ToDouble(Console.ReadLine());
            double degreeOfNumber = 1;
            for (int i=1; i<=n; i++)
            {
                degreeOfNumber *= a;
            }
            Console.WriteLine(degreeOfNumber);
            Console.ReadKey();
        }
    }
}
